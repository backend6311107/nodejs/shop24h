const express = require("express");
const app = express();
const mongoose = require("mongoose");
mongoose.set('strictQuery', true)
const port = 8000;

const productTypeRouter = require("./app/routes/productTypeRouter");
const productRouter = require("./app/routes/productRouter");
const customerRouter = require("./app/routes/customerRouter");
const orderRouter = require("./app/routes/orderRouter");
const orderDetailRouter = require("./app/routes/orderDetailController");

app.use(express.static("app"));
app.use(express.json());

app.use((req, res, next) => {
    console.log(`Current time: ${new Date()}`);
    next();
});

app.use((req, res, next) => {
    console.log(`Request method: ${req.method}`);
    next();
});

app.use("/api", productTypeRouter);
app.use("/api", productRouter);
app.use("/api", customerRouter);
app.use("/api", orderRouter);
app.use("/api", orderDetailRouter);

mongoose.connect("mongodb://127.0.0.1:27017/ex_shop24h", (error) => {
    if (error) {
        console.log(`Failed to connect to MongoDB: ${error}`);
    } else {
        console.log(`Connect MongoDB successfully !!!`);
        app.listen(port, () => {
            console.log(`App running on port: ${port}`);
        });
    }
});

