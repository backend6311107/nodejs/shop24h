const getAllOrderMiddleware = (req, res, next) => {
    console.log("Get all order middleware");
    next();
};

const getOrderByIdMiddleware = (req, res, next) => {
    console.log("Get order by ID middleware");
    next();
}

const getAllOrderOfCustomerMiddleware = (req, res, next) => {
    console.log("Get all order of customer middleware");
    next();
}

const createOrderMiddleware = (req, res, next) => {
    console.log("Create new order middleware");
    next();
};

const updateOrderByIdMiddleware = (req, res, next) => {
    console.log("Update order by ID middleware");
    next();
}

const deleteOrderByIdMiddleware = (req, res, next) => {
    console.log("Delete order by ID middleware");
    next();
}

module.exports = {
    getAllOrderMiddleware,
    getOrderByIdMiddleware,
    getAllOrderOfCustomerMiddleware,
    createOrderMiddleware,
    updateOrderByIdMiddleware,
    deleteOrderByIdMiddleware
}