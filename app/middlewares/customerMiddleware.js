const getAllCustomerMiddleware = (req, res, next) => {
    console.log("Get all customer middleware");
    next();
};

const getCustomerByIdMiddleware = (req, res, next) => {
    console.log("Get customer by ID middleware");
    next();
}

const createCustomerMiddleware = (req, res, next) => {
    console.log("Create new customer middleware");
    next();
};

const updateCustomerByIdMiddleware = (req, res, next) => {
    console.log("Update customer by ID middleware");
    next();
}

const deleteCustomerByIdMiddleware = (req, res, next) => {
    console.log("Delete customer by ID middleware");
    next();
}

module.exports = {
    getAllCustomerMiddleware,
    getCustomerByIdMiddleware,
    createCustomerMiddleware,
    updateCustomerByIdMiddleware,
    deleteCustomerByIdMiddleware
}