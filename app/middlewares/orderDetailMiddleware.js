const getAllOrderDetailMiddleware = (req, res, next) => {
    console.log("Get all order detail middleware");
    next();
}

const getOrderDetailbyIdMiddleware = (req, res, next) => {
    console.log("Get order detail by ID middleware");
    next();
}

const getAllOrderDetailofOrderMiddleware = (req, res, next) => {
    console.log("Get all order detail of order middleware");
    next();
}

const createOrderDetailMiddleware = (req, res, next) => {
    console.log("Create new order detail middleware");
    next();
};

const updateOrderDetailByIdMiddleware = (req, res, next) => {
    console.log("Update order detail by ID middleware");
    next();
}

const deleteOrderDetailByIdMiddleware = (req, res, next) => {
    console.log("Delete order detail by ID middleware");
    next();
}

module.exports = {
    getAllOrderDetailMiddleware,
    getOrderDetailbyIdMiddleware,
    getAllOrderDetailofOrderMiddleware,
    createOrderDetailMiddleware,
    updateOrderDetailByIdMiddleware,
    deleteOrderDetailByIdMiddleware
}