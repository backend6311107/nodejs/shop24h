const getAllProductMiddleware = (req, res, next) => {
    console.log("Get all product middleware.");
    next();
}

const getProductByIdMiddleware = (req, res, next) => {
    console.log("Get product by ID middleware.");
    next();
}

const createProductMiddleware = (req, res, next) => {
    console.log("Create new product middleware.");
    next();
}

const updateProductByIdMiddleware = (req, res, next) => {
    console.log("Update product by ID middleware.");
    next();
}

const deleteProductByIdMiddleware = (req, res, next) => {
    console.log("Delete product by ID middleware.");
    next();
}

module.exports = {
    getAllProductMiddleware,
    getProductByIdMiddleware,
    createProductMiddleware,
    updateProductByIdMiddleware,
    deleteProductByIdMiddleware
}