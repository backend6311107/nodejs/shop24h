const getAllProductTypeMiddleware = (req, res, next) => {
    console.log("Get all product type middleware.");
    next();
};

const getProductTypeByIdMiddleware = (req, res, next) => {
    console.log("Get product type by ID.");
    next();
};

const createProductTypeMiddleware = (req, res, next) => {
    console.log("Create new product type middleware.");
    next();
};

const updateProductTypeByIdMiddleware = (req, res, next) => {
    console.log("Update product type by ID middleware.");
    next();
}

const deleteProductTypeByIdMiddleware = (req, res, next) => {
    console.log("Delete product type by ID.");
    next();
}

module.exports = {
    getAllProductTypeMiddleware,
    getProductTypeByIdMiddleware,
    createProductTypeMiddleware,
    updateProductTypeByIdMiddleware,
    deleteProductTypeByIdMiddleware
}