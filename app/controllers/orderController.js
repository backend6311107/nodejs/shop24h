const mongoose = require("mongoose");
const orderModel = require("../models/orderModel");
const customerModel = require("../models/CustomerModel");

const getAllOrder = (req, res) => {
    orderModel.find((error, data) => {
        if (error) {
            return res.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return res.status(200).json({
            status: "Get all order successfully",
            data: data
        })
    })
};

const getOrderById = (req, res) => {
    const orderID = req.params.orderId;

    if (!mongoose.Types.ObjectId.isValid(orderID)) {
        return res.status(400).json({
            status: "Bad request",
            message: `Order ID: ${orderID} không hợp lệ !`
        })
    }

    orderModel.findById(orderID, (error, data) => {
        if (error) {
            return res.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return res.status(200).json({
            status: `Get order by ID: ${orderID} successfully.`,
            data: data
        });
    });
};

const getAllOrderOfCustomer = (req, res) => {
    const customerID = req.params.customerId;

    if (!mongoose.Types.ObjectId.isValid(customerID)) {
        return res.status(400).json({
            status: "Bad request",
            message: `Customer ID: ${customerID} không hợp lệ`
        })
    }

    customerModel.findById(customerID).populate("orders").exec((error, data) => {
        if (error) {
            return res.status(500).json({
                status: "Internal server error",
                message: error.message
            });
        }
        return res.status(200).json({
            status: "Get all order of customer successfully",
            data: data
        });
    })
};

const createOrder = (req, res) => {
    const body = req.body;
    const customerID = req.params.customerId;

    const newOrder = {
        _id: mongoose.Types.ObjectId(),
        orderDate: body.orderDate,
        shippedDate: body.shippedDate,
        note: body.note,
        cost: body.cost
    }

    orderModel.create(newOrder, (error, data) => {
        if (error) {
            return res.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        customerModel.findByIdAndUpdate(customerID, { $push: { orders: data._id } }, (err) => {
            if (err) {
                return res.status(500).json({
                    status: "Internal server error",
                    message: error.message
                })
            }
            return res.status(201).json({
                status: "Create new order successfully",
                data: data
            })
        })
    })
};

const updateOrderById = (req, res) => {
    const orderID = req.params.orderId;
    const body = req.body;

    if (!mongoose.Types.ObjectId.isValid(orderID)) {
        return res.status(400).json({
            status: "Bad request",
            message: `Order ID: ${orderID} không hợp lệ`
        });
    }

    updateOrder = {};

    updateOrder.orderDate = body.orderDate

    updateOrder.shippedDate = body.shippedDate

    updateOrder.note = body.note

    updateOrder.cost = body.cost

    orderModel.findByIdAndUpdate(orderID, updateOrder, (error, data) => {
        if (error) {
            return res.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return res.status(200).json({
            status: `Update order by ID: ${orderID} successfully.`,
            data: data
        })
    })
};

const deleteOrderById = (req, res) => {
    orderID = req.params.orderId;
    customerID = req.params.customerId;

    if (!mongoose.Types.ObjectId.isValid(orderID)) {
        return res.status(400).json({
            status: "Bad request",
            message: `orderID: ${orderID} Không hợp lệ`
        });
    }

    orderModel.findByIdAndDelete(orderID, (error, data) => {
        if (error) {
            return res.status(500).json({
                status: "Internal server error",
                message: error.message
            });
        }
        customerModel.findByIdAndUpdate(customerID, { $pull: { orders: orderID } }, (err) => {
            if (err) {
                return res.status(500).json({
                    status: "Internal server error",
                    message: error.message
                });
            }
            return res.status(200).json({
                status: `Delete order by ID: ${orderID} successfully`
            });
        })
    })
};

module.exports = {
    getAllOrder,
    getOrderById,
    getAllOrderOfCustomer,
    createOrder,
    updateOrderById,
    deleteOrderById
}