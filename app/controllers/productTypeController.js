const mongoose = require("mongoose");
const productTypeModel = require("../../app/models/productTypeModel");

const getAllProductType = (req, res) => {
    productTypeModel.find((error, data) => {
        if (error) {
            return res.status(500).json({
                status: "Internal server error",
                message: error.message
            });
        }
        return res.status(200).json({
            status: "Get all product types successfully",
            data: data
        })
    })
}

const getProductTypeById = (req, res) => {
    const productTypeID = req.params.productTypeId;

    if (!mongoose.Types.ObjectId.isValid(productTypeID)) {
        return res.status(400).json({
            status: "Bad request",
            message: `Product type ID: ${productTypeID} không hợp lệ !`
        });
    }

    productTypeModel.findById(productTypeID, (error, data) => {
        if (error) {
            return res.status(500).json({
                status: "Internal server error",
                message: error.message
            });
        }
        return res.status(200).json({
            status: `Get product type by ID: ${productTypeID} successfully.`,
            data: data
        });
    })
}

const createProductType = (req, res) => {
    const body = req.body;

    if (!body.name || body.name.trim() === "") {
        return res.status(400).json({
            status: "Bad request",
            message: "name không hợp lệ !"
        });
    }

    const newProductType = {
        _id: mongoose.Types.ObjectId(),
        name: body.name,
        description: body.description
    };

    productTypeModel.create(newProductType, (error, data) => {
        if (error) {
            return res.status(500).json({
                status: "Internal server error",
                message: error.message
            });
        }
        return res.status(201).json({
            status: "Create new Product Type successfully.",
            data: data
        });
    })
}

const updateProductTypeById = (req, res) => {
    const productTypeID = req.params.productTypeId;
    const body = req.body;

    if (!mongoose.Types.ObjectId.isValid(productTypeID)) {
        return res.status(400).json({
            status: "Bad request",
            message: `Product type ID: ${productTypeID} không hợp lệ !`
        });
    }
    if (body.name !== undefined && body.name.trim() === "") {
        return res.status(400).json({
            status: "Bad request",
            message: "name không hợp lệ !"
        });
    }

    const updateProductType = {};
    if (body.name !== undefined) {
        updateProductType.name = body.name;
    }
    if (body.description !== undefined) {
        updateProductType.description = body.description;
    }
    productTypeModel.findByIdAndUpdate(productTypeID, updateProductType, (error, data) => {
        if (error) {
            return res.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return res.status(200).json({
            status: `Update product type by ID: ${productTypeID} successfully.`,
            data: data
        })
    })
}

const deleteProductTypeById = (req, res) => {
    const productTypeID = req.params.productTypeId;

    if (!mongoose.Types.ObjectId.isValid(productTypeID)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `Product type ID: ${productTypeID} không hợp lệ !`
        });
    }

    productTypeModel.findByIdAndDelete(productTypeID, (error) => {
        if (error) {
            return res.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return res.status(200).json({
            status: `Delete product type by ID: ${productTypeID} successfully.`
        })
    })
}

module.exports = {
    getAllProductType,
    getProductTypeById,
    createProductType,
    updateProductTypeById,
    deleteProductTypeById
}