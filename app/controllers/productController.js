const mongoose = require("mongoose");
const productModel = require("../models/productModel");
const productTypeModel = require("../models/productTypeModel");

const getAllProduct = (req, res) => {
    productModel.find((error, data) => {
        if (error) {
            return res.status(500).json({
                status: "Internal server error",
                message: error.message
            });
        }
        return res.status(200).json({
            status: "Get all product successfully.",
            data: data
        });
    });
};

const getAllProductById = (req, res) => {
    const productID = req.params.productId;

    if (!mongoose.Types.ObjectId.isValid(productID)) {
        return res.status(400).json({
            status: "Bad request",
            message: `Product ID: ${productID} không hợp lệ !`
        });
    }

    productModel.findById(productID, (error, data) => {
        if (error) {
            return res.status(500).json({
                status: "Internal server error.",
                message: error.message
            });
        }
        return res.status(200).json({
            status: `Get product by ID: ${productID} successfully.`,
            data: data
        });
    })
};

const createProduct = async (req, res) => {
    const body = req.body;

    if (!body.name || body.name.trim() === "") {
        return res.status(400).json({
            status: "Bad request",
            message: `name Không hợp lệ !`
        });
    }
    if (body.description) {
        if (!isNaN(body.descriptions)) {
            return res.status(400).json({
                status: "Bad request",
                message: "description không hợp lệ !"
            });
        }
    }
    if (!body.type || body.type.trim() === "") {
        return res.status(400).json({
            status: "Bad request",
            message: "type không hợp lệ !"
        })
    }
    if (!mongoose.Types.ObjectId.isValid(body.type)) {
        return res.status(400).json({
            status: "Bad request",
            message: `type ID: ${body.type} không hợp lệ !`
        });
    }
    //const typeID = await productTypeModel.findOne({ _id: body.type });
    // if(!typeID){
    //     return res.status(400).json({
    //         status: "Bad request",
    //         message: `Type ID: ${typeID} không hợp lệ !`
    //     })
    // }
    if (!body.imageUrl || body.imageUrl.trim() === "") {
        return res.status(400).json({
            status: "Bad request",
            message: "imageUrl Không hợp lệ !"
        });
    }
    if (!body.buyPrice || isNaN(body.buyPrice) || body.buyPrice < 0) {
        return res.status(400).json({
            status: "Bad request",
            message: "buyPrice Không hợp lệ !"
        });
    }
    if (!body.promotionPrice || isNaN(body.promotionPrice) || body.promotionPrice < 0) {
        return res.status(400).json({
            status: "Bad request",
            message: "promotionPrice không hợp lệ !"
        });
    }
    if (body.amount) {
        if (isNaN(body.amount) || body.amount < 0) {
            return res.status(400).json({
                status: "Bad request",
                message: "amount không hợp lệ !"
            });
        }
    }
    const newProduct = {
        _id: mongoose.Types.ObjectId(),
        name: body.name,
        description: body.description,
        type: mongoose.Types.ObjectId(body.type),
        imageUrl: body.imageUrl,
        buyPrice: body.buyPrice,
        promotionPrice: body.promotionPrice,
        amount: body.amount
    }
    productModel.create(newProduct, (error, data) => {
        if (error) {
            return res.status(500).json({
                status: "Internal server error",
                message: error.message
            });
        }
        return res.status(201).json({
            status: "Create new product successfully.",
            newProduct
        })
    })
};

const updateProductById = (req, res) => {
    const productID = req.params.productId;
    const body = req.body;

    if (!mongoose.Types.ObjectId.isValid(productID)) {
        return res.status(400).json({
            status: "Bad request",
            message: `Product ID: ${productID} không hợp lệ !`
        });
    }
    if (body.name !== undefined && body.name.trim() === "") {
        return res.status(400).json({
            status: "Bad request",
            message: "name Không hợp lệ !"
        });
    }

    if (body.type !== undefined && body.type.trim() === "") {
        return res.status(400).json({
            status: "Bad request",
            message: "type Không hợp lệ !"
        });
    }
    if (body.type) {
        if (!mongoose.Types.ObjectId.isValid(body.type)) {
            return res.status(400).json({
                status: "Bad request",
                message: `type: ${body.type} không hợp lệ !`
            });
        }
    }
    if (body.imageUrl !== undefined && body.imageUrl.trim() === "") {
        return res.status(400).json({
            status: "Bad request",
            message: "imageUrl Không hợp lệ !"
        });
    }
    if (body.buyPrice !== undefined && !(isNaN(body.buyPrice) || body.buyPrice > 0)) {
        return res.status(400).json({
            status: "Bad request",
            message: "buyPrice không hợp lệ !"
        })
    }
    if (body.promotionPrice !== undefined && !(isNaN(body.promotionPrice) || body.promotionPrice > 0)) {
        return res.status(400).json({
            status: "Bad request",
            message: "promotionPrice không hợp lệ !"
        })
    }

    if (body.amount) {
        if (isNaN(body.amount) || body.amount < 0) {
            return res.status(400).json({
                status: "Bad request",
                message: "amount không hợp lệ !"
            });
        }
    }

    const updateProduct = {};
    updateProduct.name = body.name
    updateProduct.description = body.description
    updateProduct.type = body.type
    updateProduct.imageUrl = body.imageUrl
    updateProduct.buyPrice = body.buyPrice
    updateProduct.promotionPrice = body.promotionPrice
    updateProduct.amount = body.amount

    console.log(updateProduct);
    productModel.findByIdAndUpdate(productID, updateProduct, (error, data) => {
        if (error) {
            return res.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return res.status(200).json({
            status: `Update product by ID: ${productID} successfully`,
            data: data
        })
    })
};

const deleteProductById = (req, res) => {
    const productID = req.params.productId;

    if (!mongoose.Types.ObjectId.isValid(productID)) {
        return res.status(400).json({
            status: "Bad request",
            message: `Product ID: ${productID} không hợp lệ !`
        });
    }

    productModel.findByIdAndDelete(productID, (error) => {
        if (error) {
            return res.status(500).json({
                status: "Internal server error",
                message: error.message
            });
        }
        return res.status(200).json({
            status: `Delete product by ID: ${productID} successfully.`
        })
    });
};

module.exports = {
    getAllProduct,
    getAllProductById,
    createProduct,
    updateProductById,
    deleteProductById
}