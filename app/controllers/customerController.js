const mongoose = require("mongoose");
const { updateMany } = require("../models/CustomerModel");
const customerModel = require("../models/CustomerModel");

const getAllCustomer = (req, res) => {
    customerModel.find((error, data) => {
        if (error) {
            return res.status(500).json({
                status: "Internal server error",
                message: error.message
            });
        }
        return res.status(200).json({
            status: "Get all customer successfully.",
            data: data
        });
    });
};

const getCustomerById = (req, res) => {
    const customerID = req.params.customerId;

    if (!mongoose.Types.ObjectId.isValid(customerID)) {
        return res.status(400).json({
            status: "Bad request",
            message: `Customer ID: ${customerID} không hợp lệ !`
        });
    }

    customerModel.findById(customerID, (error, data) => {
        if (error) {
            return res.status(500).json({
                status: "Internal server error",
                message: error.message
            });
        }
        return res.status(200).json({
            status: `Get customer by ID: ${customerID} successfully`,
            data: data
        })
    })
}

const createCustomer = (req, res) => {
    const body = req.body;

    if (!body.fullName) {
        return res.status(400).json({
            status: "Bad requset",
            message: "fullName không hợp lệ !"
        });
    }
    if (!body.phone) {
        return res.status(400).json({
            status: "Bad request",
            message: "phone không hợp lệ !"
        });
    }
    if (!body.email) {
        return res.status(400).json({
            status: "Bad request",
            message: "email không hợp lệ !"
        });
    }

    const newCustomer = {
        _id: mongoose.Types.ObjectId(),
        fullName: body.fullName,
        phone: body.phone,
        email: body.email,
        address: body.address,
        city: body.city,
        country: body.country,
        orders: body.orders
    }
    customerModel.create(newCustomer, (error, data) => {
        if (error) {
            return res.status(500).json({
                status: "Internal server error",
                message: error.message
            });
        }
        return res.status(201).json({
            status: "Create new customer successfully.",
            data: data
        });
    })
};

const updateCustomerById = (req, res) => {
    const customerID = req.params.customerId;
    const body = req.body;

    if (!mongoose.Types.ObjectId.isValid(customerID)) {
        return res.status(400).json({
            status: "Bad request",
            message: `Customer ID: ${customerID} Không hợp lệ`
        });
    }
    if (body.fullName !== undefined && body.fullName.trim() === "") {
        return res.status(400).json({
            status: "Bad request",
            message: `fullName Không hợp lệ`
        });
    }
    if (body.phone !== undefined && body.phone.trim() === "") {
        return res.status(400).json({
            status: "Bad request",
            message: `phone Không hợp lệ`
        });
    }
    if (body.email !== undefined && body.email.trim() === "") {
        return res.status(400).json({
            status: "Bad request",
            message: `email Không hợp lệ`
        });
    }

    const updateCustomer = {};
    updateCustomer.fullName = body.fullName
    updateCustomer.phone = body.phone
    updateCustomer.email = body.email
    updateCustomer.address = body.address
    updateCustomer.city = body.city
    updateCustomer.country = body.country
    customerModel.findByIdAndUpdate(customerID, updateCustomer, (error, data) => {
        if (error) {
            return res.status(500).json({
                status: "Internal server error",
                message: error.message
            });
        }
        return res.status(200).json({
            status: `Update customer ID: ${customerID} successfully`,
            data: data
        });
    })
};

const deleteCustomerById = (req, res) => {
    const customerID = req.params.customerId;

    if (!mongoose.Types.ObjectId.isValid(customerID)) {
        return res.status(400).json({
            status: "Bad request",
            message: `Customer ID: ${customerID} không hợp lệ`
        });
    }

    customerModel.findByIdAndDelete(customerID, (error, data) => {
        if (error) {
            return res.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return res.status(200).json({
            status: `Delete customer by ID: ${customerID} successfully.`
        })
    })
};

module.exports = {
    getAllCustomer,
    getCustomerById,
    createCustomer,
    updateCustomerById,
    deleteCustomerById
}