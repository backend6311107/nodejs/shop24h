const mongoose = require("mongoose");
const { populate } = require("../models/orderDetailModel");
const orderDetailModel = require("../models/orderDetailModel");
const orderModel = require("../models/orderModel");
const productModel = require("../models/productModel");


const getAllOrderDetail = (req, res) => {
    orderDetailModel.find((error, data) => {
        if (error) {
            return res.status(500).json({
                status: "Internal server error",
                message: error.message
            });
        }
        return res.status(200).json({
            status: "Get all order detail successfully",
            data: data
        });
    });
};

const getOrderDetailbyId = (req, res) => {
    const orderDetailID = req.params.orderdetailid;

    if (!mongoose.Types.ObjectId.isValid(orderDetailID)) {
        return res.status(400).json({
            status: "Bad request",
            message: `Order detail ID: ${orderDetailID} không hợp lệ`
        });
    }

    orderDetailModel.findById(orderDetailID, (error, data) => {
        if (error) {
            return res.status.json({
                status: "Internal server error",
                message: error.message
            });
        }
        return res.status(200).json({
            status: `Get order detail by ID: ${orderDetailID} successfully`,
            data: data
        })
    })
}

const getAllOrderDetailofOrder = (req, res) => {
    const orderID = req.params.orderID;

    if (!mongoose.Types.ObjectId.isValid(orderID)) {
        return res.status(400).json({
            status: "Bad request",
            message: `order ID: ${orderID} không hợp lệ`
        })
    }
    //Lấy ra dữ liệu, trường product chỉ có ID product
    // orderModel.findById(orderID).populate("orderDetail").exec((error, data) => {
    //     if (error) {
    //         return res.status(500).json({
    //             status: "Internal server error",
    //             messgae: error.message
    //         });
    //     }
    //     return res.status(200).json({
    //         status: "Get all order detail of customer successfully",
    //         data: data
    //     });
    // });

    //Lấy ra dữ liệu orderdetail of order, trường product có id, name product
    orderModel.findById(orderID).populate({
        path: "orderDetail",
        populate: {
            path: "product",
            //model: "Product"  // lấy tất cả trường của model product
            select: "name" // Chọn duy nhất trường name của model product
        }
    }).exec((error, data) => {
        if (error) {
            return res.status(500).json({
                status: "Internal server error",
                messgae: error.message
            });
        }
        return res.status(200).json({
            status: "Get all order detail of customer successfully",
            data: data
        });
    });
};

const createOrderDetail = (req, res) => {
    const body = req.body;
    const orderID = req.params.orderId;

    if (!mongoose.Types.ObjectId.isValid(orderID)) {
        return res.status(400).json({
            status: "Bad request",
            message: `orderID Không hợp lệ !`
        });
    }

    if (!mongoose.Types.ObjectId.isValid(body.product)) {
        return res.status(400).json({
            status: "Bad request",
            message: `product Không hợp lệ !`
        });
    }

    if (body.quantity) {
        if (isNaN(body.quantity) || body.quantity < 0) {
            return res.status(400).json({
                status: "Bad request",
                message: `quantity không hợp lệ !`
            });
        }
    }

    const newOrderDetail = {
        _id: mongoose.Types.ObjectId(),
        product: mongoose.Types.ObjectId(body.product),
        quantity: body.quantity
    }

    orderDetailModel.create(newOrderDetail, (error, data) => {
        if (error) {
            return res.status(500).json({
                status: "Internal server error",
                message: error.message
            });
        }
        orderModel.findByIdAndUpdate(orderID, { $push: { orderDetail: data._id } }, (err, data) => {
            if (err) {
                return res.status(500).json({
                    status: "Internal server error",
                    message: err.message
                });
            }
            return res.status(201).json({
                status: "Create new order detail successfully",
            })
        });
    })
};

const updateOrderDetailById = (req, res) => {
    const orderDetailID = req.params.orderdetailid;
    const body = req.body;

    if (!mongoose.Types.ObjectId.isValid(orderDetailID)) {
        return res.status(400).json({
            status: "Bad request",
            message: `Order Detail ID: ${orderDetailID} không hợp lệ.`
        })
    }
    if (body.product) {
        if (!mongoose.Types.ObjectId.isValid(body.product)) {
            return res.status(400).json({
                status: "Bad request",
                message: `Product ID: ${body.product} không hợp lệ`
            });
        }
    }
    if (body.quantity !== undefined && (isNaN(body.quantity) || body.quantity < 0)) {
        return res.status(400).json({
            status: "Bad request",
            message: "quantity không hợp lệ !"
        })
    }

    const updateOrderDetail = {};
    updateOrderDetail.product = body.product
    updateOrderDetail.quantity = body.quantity

    orderDetailModel.findByIdAndUpdate(orderDetailID, updateOrderDetail, (error, data) => {
        if (error) {
            return res.status(500).json({
                status: `Internal server error`,
                message: error.message
            })
        }
        return res.status(200).json({
            status: `Update order detail by ID: ${orderDetailID} successfully`,
            data: data
        })
    })
};

const deleteOrderDetailById = (req, res) => {
    const orderDetailID = req.params.orderdetailid;
    const orderID = req.params.orderId;

    if (!mongoose.Types.ObjectId.isValid(orderID)) {
        return res.status(400).json({
            status: "Bad request",
            messgae: `Order ID: ${orderID} không hợp lệ`
        })
    }
    if (!mongoose.Types.ObjectId.isValid(orderDetailID)) {
        return res.status(400).json({
            status: "Bad request",
            message: `Order Detail ID: ${orderDetailID} không hợp lệ`
        })
    }

    orderDetailModel.findByIdAndDelete(orderDetailID, (error, data) => {
        if (error) {
            return res.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        orderModel.findByIdAndUpdate(orderID, { $pull: { orderDetail: orderDetailID } }, (err) => {
            if (err) {
                return res.status(500).json({
                    status: "Internal server error",
                    message: err.message
                })
            }
            return res.status(200).json({
                status: `Delete order detail by ID: ${orderDetailID} successfully`
            })
        })
    })
};

module.exports = {
    getAllOrderDetail,
    getOrderDetailbyId,
    getAllOrderDetailofOrder,
    createOrderDetail,
    updateOrderDetailById,
    deleteOrderDetailById
}