const express = require("express");
const router = express.Router();
const productTypeMiddleware = require("../middlewares/productTypeMiddleware");
const productTypeController = require("../controllers/productTypeController");

router.get("/productTypes", productTypeMiddleware.getAllProductTypeMiddleware, productTypeController.getAllProductType);

router.get("/productTypes/:productTypeId", productTypeMiddleware.getProductTypeByIdMiddleware, productTypeController.getProductTypeById);

router.post("/productTypes", productTypeMiddleware.createProductTypeMiddleware, productTypeController.createProductType);

router.put("/productTypes/:productTypeId", productTypeMiddleware.updateProductTypeByIdMiddleware, productTypeController.updateProductTypeById);

router.delete("/productTypes/:productTypeId", productTypeMiddleware.deleteProductTypeByIdMiddleware, productTypeController.deleteProductTypeById);

module.exports = router;