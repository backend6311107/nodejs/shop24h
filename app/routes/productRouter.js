const express = require("express");
const router = express.Router();
const productMiddleware = require("../middlewares/productMiddleware");
const productController = require("../controllers/productController");

router.get("/products", productMiddleware.getAllProductMiddleware, productController.getAllProduct);

router.get("/products/:productId", productMiddleware.getProductByIdMiddleware, productController.getAllProductById);

router.post("/products", productMiddleware.createProductMiddleware, productController.createProduct);

router.put("/products/:productId", productMiddleware.updateProductByIdMiddleware, productController.updateProductById);

router.delete("/products/:productId", productMiddleware.deleteProductByIdMiddleware, productController.deleteProductById);

module.exports = router;