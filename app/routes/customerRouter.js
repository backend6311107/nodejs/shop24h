const express = require("express");
const router = express.Router();
const customerMiddleware = require("../middlewares/customerMiddleware");
const customerController = require("../controllers/customerController");

router.get("/customers", customerMiddleware.getAllCustomerMiddleware, customerController.getAllCustomer);

router.get("/customers/:customerId", customerMiddleware.getCustomerByIdMiddleware, customerController.getCustomerById);

router.post("/customers", customerMiddleware.createCustomerMiddleware, customerController.createCustomer);

router.put("/customers/:customerId", customerMiddleware.updateCustomerByIdMiddleware, customerController.updateCustomerById);

router.delete("/customers/:customerId", customerMiddleware.deleteCustomerByIdMiddleware, customerController.deleteCustomerById);

module.exports = router;