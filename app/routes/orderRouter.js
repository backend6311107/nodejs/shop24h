const express = require("express");
const router = express.Router();
const orderMiddleware = require("../middlewares/orderMiddleware");
const orderController = require("../controllers/orderController");

router.get("/orders", orderMiddleware.getAllOrderMiddleware, orderController.getAllOrder);

router.get("/orders/:orderId", orderMiddleware.getOrderByIdMiddleware, orderController.getOrderById);

router.get("/customers/:customerId/orders", orderMiddleware.getAllOrderOfCustomerMiddleware, orderController.getAllOrderOfCustomer);

router.post("/customers/:customerId/orders", orderMiddleware.createOrderMiddleware, orderController.createOrder);

router.put("/orders/:orderId", orderMiddleware.updateOrderByIdMiddleware, orderController.updateOrderById);

router.delete("/customers/:customerId/orders/:orderId", orderMiddleware.updateOrderByIdMiddleware, orderController.deleteOrderById);

module.exports = router;