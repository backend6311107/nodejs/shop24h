const express = require("express");
const router = express.Router();
const orderDetailMiddleware = require("../middlewares/orderDetailMiddleware");
const orderDetailController = require("../controllers/orderDetailController");

router.get("/orderdetails", orderDetailMiddleware.getAllOrderDetailMiddleware, orderDetailController.getAllOrderDetail);

router.get("/orderdetails/:orderdetailid", orderDetailMiddleware.getOrderDetailbyIdMiddleware, orderDetailController.getOrderDetailbyId);

router.get("/orders/:orderID/orderdetails", orderDetailMiddleware.getAllOrderDetailofOrderMiddleware, orderDetailController.getAllOrderDetailofOrder);

router.post("/orders/:orderId/orderdetails", orderDetailMiddleware.createOrderDetailMiddleware, orderDetailController.createOrderDetail);

router.put("/orderdetails/:orderdetailid", orderDetailMiddleware.updateOrderDetailByIdMiddleware, orderDetailController.updateOrderDetailById);

router.delete("/orders/:orderId/orderdetails/:orderdetailid", orderDetailMiddleware.deleteOrderDetailByIdMiddleware, orderDetailController.deleteOrderDetailById);

module.exports = router;