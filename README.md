# Shop24h
Shop24h project is a NodeJS backend project that consists of 5 model: Customer, product, productType, order, orderDetail and it is connected to a MongoDB database. The project provides REST API functionalities, including GET, POST, PUT, and DELETE requests.

## Installation
After opening the project, install the following commands in the terminal:  
npm install  
Please go to the index.js file and update the mongoose.connect connection string to match your machine

## Usage
Run the command 'npm start' in the terminal to start the project, and use Postman to perform queries and test the APIs.